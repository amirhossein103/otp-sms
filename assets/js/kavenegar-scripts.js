const $k_doc = jQuery(document),
    $k_window = jQuery(window),
    resendLink = jQuery('#resend-sms');
$k_doc.ready(function ($) {
    'use strict'

    jQuery('button[name="send_otp"]').on('click', function (e) {
        e.preventDefault();

        const $mobile = jQuery('input[name="mobile"]').val(),
            $password = jQuery('input[name="password"]'),
            noticesWrapper = jQuery('.woocommerce-notices-wrapper');

        let $username = jQuery('input[name="username"]'),
            startProcess= false;

        if ($username.length && $username.val !== '') {
            $username = $username.val();
        }else {
            $username="";
        }
        if ($password.length && ($password.val() === '' || $password.val === ' ')) {
            $password.focus();
        } else {
            jQuery.ajax({
                url: kavenegar.ajax_url,
                type: 'post',
                dataType: 'html',
                data: {
                    action: 'validate_and_action',
                    mobile: $mobile,
                    username: $username,
                    security: kavenegar.otp_nonce
                },
                beforeSend: function () {
                    jQuery('.mrgn-form-login').addClass('blockUI blockOverlay')
                },
                success: function (response) {

                    if (response === '1' || response==="2") {
                        $('#reg_mobile').parent().css('display','none');
                        $('.password-input').parent().css('display','none');
                        $('.woocommerce-privacy-policy-text').css('display','none');
                        $('button[name="send_otp"]').parent().css('display','none');
                        $('input[name="username"]').parent().css('display','none');
                        noticesWrapper.html('');

                        $('#otp_sms').parent().css('display','block');
                        $('.resend-otp').css('display','flex');
                        $('button[name="register"]').parent().css('display','block');

                        if (response==='2'){
                            noticesWrapper.append('<div class="alert alert-error" role="alert">ارسال مجدد کد تا پایان مدت زمان تعیین شده امکان پذیر نمیباشد.</div>');
                        }
                        startProcess=true;
                    } else
                        noticesWrapper.html(response);

                },
                error: function () {
                },
                complete: function () {
                    jQuery('.mrgn-form-login').removeClass('blockUI blockOverlay');
                    if (startProcess){
                        theTimeLeft($mobile);
                    }
                }
            });
        }

    });

    jQuery('button[name="login_otp"]').on('click', function (e) {
        e.preventDefault();

        const $mobile = jQuery('input[name="mobile"]').val(),
            noticesWrapper = jQuery('.woocommerce-notices-wrapper');

        let loginProcess= false;


        jQuery.ajax({
                url: kavenegar.ajax_url,
                type: 'post',
                dataType: 'html',
                data: {
                    action: 'check_login_info',
                    mobile: $mobile,
                    security: kavenegar.otp_nonce
                },
                beforeSend: function () {
                    jQuery('.mrgn-form-login').addClass('blockUI blockOverlay')
                },
                success: function (response) {

                    if (response === '1' || response==="2") {
                        $('#mobile').parent().css('display','none');
                        $('button[name="login_otp"]').parent().css('display','none');
                        $('#login-by-password').css('display','none');
                        noticesWrapper.html('');

                        $('#otp_sms').parent().css('display','block');
                        $('.resend-otp').css('display','flex');
                        $('button[name="login"]').css('display','block');

                        if (response==='2'){
                            noticesWrapper.append('<div class="alert alert-error" role="alert">ارسال مجدد کد تا پایان مدت زمان تعیین شده امکان پذیر نمیباشد.</div>');
                        }
                        loginProcess=true;
                    } else
                        noticesWrapper.html(response);

                },
                error: function () {
                },
                complete: function () {
                    jQuery('.mrgn-form-login').removeClass('blockUI blockOverlay');
                    if (loginProcess){
                        theTimeLeft($mobile);
                    }
                }
            });


    });

    resendLink.on('click', function (e){
        const $mobile = jQuery('input[name="mobile"]').val(),
            $noticesWrap= jQuery('.woocommerce-notices-wrapper'),
            $this = jQuery(this);

        if (!kavenegar.flag || $this.attr('data-action')==='0'){
            return false;
        }

        jQuery.ajax({
            url: kavenegar.ajax_url,
            type: 'post',
            dataType: 'html',
            data: {
                action: 'resend_sms',
                mobile: $mobile,
            },
            beforeSend: function () {
                jQuery('#resend-sms .loading-icon').css('display','inline-block')
            },
            success: function (response) {
                $noticesWrap.html('');
                if (response === '1') {
                    kavenegar.flag = false;
                    resendLink.removeClass('enable-resend').addClass('disabled-resend').attr('data-action','0');
                    jQuery('#count__down').html('239');
                    countDownResend($mobile);
                    $noticesWrap.append('<div class="alert alert-success" role="alert">کد تایید با موفقیت ارسال شد.</div>');
                } else if (response === "0") {
                    kavenegar.flag = true;
                    resendLink.removeClass('disabled-resend').addClass('enable-resend').attr('data-action','1');
                    $noticesWrap.append('<div class="alert alert-error" role="alert">خطا در ارسال مجدد. لطفا دقایقی دیگر دوباره تلاش کنید.</div>');
                }
            },
            error: function () {
                alert('خطا در ارسال اطلاعات');
            },
            complete: function () {
                jQuery('#resend-sms .loading-icon').css('display','none');
            }
        });
    });

    $('input[name="otp_sms"]').keypress(function (e) {
        if (e.which == 13) {
            $(this).blur();
            return false;
        }
    });
});

function theTimeLeft($mobile){
    jQuery.ajax({
        url: kavenegar.ajax_url,
        type: 'post',
        dataType: 'html',
        data: {
            action: 'resend_otp',
            mobile: $mobile,
        },
        beforeSend: function () {
            jQuery('.resend-otp').css('opacity','.7')
        },
        success: function (response) {
            jQuery('#count__down').html(response);
        },
        error: function () {
        },
        complete: function () {
            jQuery('.resend-otp').css('opacity','1');
            countDownResend($mobile);
        }
    });
}
function countDownResend($mobile){
    let countDown = jQuery('#count__down'),
        timeLeft = countDown.text();
    const resendTimer = setInterval(function () {
        if (timeLeft <= 0) {
            clearInterval(resendTimer);
            countDown.html('0');
            jQuery.ajax({
                url:kavenegar.ajax_url,
                type:"post",
                dataType:'html',
                data:{
                    action:'the_resend_link',
                    mobile:$mobile
                },
                beforeSend: function () {
                    resendLink.css('opacity','.7');
                },
                success: function (response) {
                    if (response==="1"){
                        kavenegar.flag=true;
                        resendLink.removeClass('disabled-resend').addClass('enable-resend').attr('data-action','1');
                    }else if(response==='0'){
                        kavenegar.flag=false;
                        resendLink.removeClass('enable-resend').addClass('disabled-resend').attr('data-action','0');
                    }
                },
                error: function () {
                },
                complete: function () {
                    resendLink.css('opacity','1');
                }

            })
        } else {
            countDown.html(timeLeft);
        }
        timeLeft -= 1;
    }, 1000);
}