<?php

use Kavenegar\KavenegarApi;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;


/**
 * Filter the form login template path to use form-login.php in this plugin instead of the one in WooCommerce.
 *
 * @param string $template Default template file path.
 * @param string $template_name Template file slug.
 * @param string $template_path Template file name.
 *
 * @return string The new Template file path.
 */
add_filter( 'woocommerce_locate_template', 'intercept_wc_template', 10, 3 );
function intercept_wc_template( $template, $template_name, $template_path ) {

	$template_directory = ( new KaveNegar_Handler )->plugin_path( 'woocommerce/' );
	$path               = $template_directory . $template_name;

	return file_exists( $path ) ? $path : $template;

}

function kavenegar_plugin_path(): string {

	// gets the absolute path to this plugin directory
	return ABSPATH . str_replace( site_url() . "/", "", plugins_url() ) . "/";

}

/**
 * Sanitize phone number.
 * Allows only numbers and "+" (plus sign).
 *
 * @param string $phone Phone number.
 *
 * @return string
 * @since 1.0.0
 */
function kavenegar_sanitize_phone_number( string $phone ): string {
	return preg_replace( '/[^\d+]/', '', $phone );
}

function kavenegar_get_otp(): string {

	$code = "";
	for ( $i = 0; $i <= 4; $i ++ ) {
		$code .= rand( 0, 9 );
	}

	return $code;
}

function kavenegar_build_same_num( $mobile ) {
	$correct_nom = false;
	if ( str_starts_with( $mobile, '+989' ) && strlen( $mobile ) == 13 ) {

		$correct_nom = true;

		$mobile = substr_replace( $mobile, '', 0, 3 );

	} elseif ( str_starts_with( $mobile, '00989' ) && strlen( $mobile ) == 14 ) {

		$correct_nom = true;

		$mobile = substr_replace( $mobile, '', 0, 4 );

	} elseif ( str_starts_with( $mobile, '09' ) && strlen( $mobile ) == 11 ) {

		$correct_nom = true;

		$mobile = substr_replace( $mobile, '', 0, 1 );

	} elseif ( str_starts_with( $mobile, '9' ) && strlen( $mobile ) == 10 ) {

		$correct_nom = true;

	}

	if ( $correct_nom ) {
		return $mobile;
	}

	return false;
}

function kavenegar_send_sms( $receptor ): void {

	try {
		$api      = new KavenegarApi( "76335256796844364564344E37596C4E4639784B446D695067496D75505051475234675373634F397857383D" );
		$token    = kavenegar_get_otp();
		$token2   = "";
		$token3   = "";
		$template = "loginVerify";
		$type     = "sms";//sms | call
		$result   = $api->VerifyLookup( $receptor, $token, $token2, $token3, $template, $type );
		log_result( kavenegar_sanitize_phone_number( $receptor ), $token );
	} catch ( ApiException $e ) {
		echo $e->errorMessage();
	} catch ( HttpException $e ) {
		echo $e->errorMessage();
	}
}

add_action( 'woocommerce_created_customer', 'kavenegar_new_user_info', 10, 3 );
function kavenegar_new_user_info( $customer_id, $new_customer_data, $password_generated ): void {

	kavenegar_send_registration_info( $new_customer_data['user_mobile'], $new_customer_data['user_login'], $new_customer_data['user_pass'] );

}

function kavenegar_send_registration_info( $mobile, $username, $password ): void {

	try {
		$api      = new KavenegarApi( "76335256796844364564344E37596C4E4639784B446D695067496D75505051475234675373634F397857383D" );
		$token    = $username;
		$token2   = $mobile;
		$token3   = $password;
		$template = "RegistrationInfo";
		$type     = "sms";//sms | call
		$result   = $api->VerifyLookup( $mobile, $token, $token2, $token3, $template, $type );
	} catch ( ApiException $e ) {
		echo $e->errorMessage();
	} catch ( HttpException $e ) {
		echo $e->errorMessage();
	}

}

function log_result( $receptor, $token ): void {
	global $wpdb;
	$wpdb->insert(
		$wpdb->prefix . "otp_sms",
		array(
			'date'      => current_datetime()->format( 'Y-m-d H:i:s' ),
			'recipient' => $receptor,
			'token'     => $token
		)
	);
}

function kavenegar_billing_phone_exist( $mobile ): bool {

	global $wpdb;

	// use $wpdb to make a query directly to the database
	$results = $wpdb->get_col( "
        SELECT DISTINCT `meta_value` FROM `{$wpdb->prefix}usermeta` WHERE `meta_key` = 'billing_phone' and `meta_value` = '{$mobile}';
    " );

	return ! empty( $results );
}

function kavenegar_build_standard_num( $mobile ): string {
	return '0' . kavenegar_build_same_num( $mobile );
}

function kavenegar_get_user_by_billing_phone( $mobile ) {
	global $wpdb;

	$user_id = $wpdb->get_col( "SELECT DISTINCT `user_id` FROM `{$wpdb->prefix}usermeta` WHERE `meta_key` = 'billing_phone' and `meta_value` = '{$mobile}' ORDER BY user_id DESC LIMIT 1" );
	if ( empty( $user_id ) ) {
		return false;

	}

	$user = get_userdata( $user_id[0] );

	return $user->user_login;
}

function kavenegar_arabic_persian_filter( $mobile ) {

	$fromchar = array( '۰','۱','۲','۳','۴','۵','۶','۷','۸','۹','٠','١','٢','٣','٤','٥','٦','٧','٨','٩' );

	$num = array( '0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9' );

	return trim(str_replace( $fromchar, $num, $mobile ));

}

function kavenegar_is_valid_otp( $otp_code, $mobile ): bool {

	$otp_code = kavenegar_arabic_persian_filter( $otp_code );

	$has_time_limit = kavenegar_in_time_limit( $mobile,true );

	if ( ! $has_time_limit ) {
		return false;
	}

	if ( $has_time_limit[0] !== $otp_code ) {
		return false;
	}

	return true;

}

function kavenegar_fetch_otp_fields( $mobile ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "otp_sms";

	$latest_otp = $wpdb->get_results( "SELECT * FROM `{$table_name}` WHERE recipient = '{$mobile}' ORDER BY Id DESC LIMIT 1" );

	if ( empty( $latest_otp ) ) {
		return false;
	}

	$otp_obj = $latest_otp[0];
	if ( empty( $otp_obj ) || ! is_object( $otp_obj ) ) {
		return false;
	}

	if ( empty( $otp_obj->ID ) || empty( $otp_obj->recipient ) || empty( $otp_obj->date ) || empty( $otp_obj->token ) ) {
		return false;
	}

	return $otp_obj;
}

function kavenegar_in_time_limit( $mobile, $properties = false ) {


	$otp_obj = kavenegar_fetch_otp_fields( $mobile );

	if ( ! $otp_obj ) {
		return false;
	}

	$time_difference = 4 * 60;
	$sent_time       = strtotime( $otp_obj->date );
	$current_time    = strtotime( current_datetime()->format( 'Y-m-d H:i:s' ) );
	$time_spent      = abs( $current_time - $sent_time );

	if ( $time_spent > $time_difference ) {
		return false;
	}

	if ( $properties ) {
		return [$otp_obj->token, $sent_time];
	}

	return true;
}

/**
 * Authenticates a user, confirming the username and password are valid.
 *
 * @since 2.8.0
 *
 * @param WP_User|WP_Error|null $user     WP_User or WP_Error object from a previous callback. Default null.
 * @param string                $username Username for authentication.
 * @param string                $password Password for authentication.
 * @return WP_User|WP_Error WP_User on success, WP_Error on failure.
 */
function kavenegar_authenticate_username_otp( $user, $username, $password ) {
	if ( $user instanceof WP_User ) {
		return $user;
	}

	if ( empty( $username ) || empty( $password ) ) {
		if ( is_wp_error( $user ) ) {
			return $user;
		}

		$error = new WP_Error();

		if ( empty( $username ) ) {
			$error->add( 'empty_username', __( '<strong>Error</strong>: The username field is empty.' ) );
		}

		if ( empty( $password ) ) {
			$error->add( 'empty_password', __( '<strong>خطا</strong>: کد تایید وارد نشده است.' ) );
		}

		return $error;
	}

	$user = get_user_by( 'login', $username );

	if ( ! $user ) {
		return new WP_Error(
			'invalid_username',
			sprintf(
			/* translators: %s: User name. */
				__( '<strong>Error</strong>: The username <strong>%s</strong> is not registered on this site. If you are unsure of your username, try your email address instead.' ),
				$username
			)
		);
	}

	/**
	 * Filters whether the given user can be authenticated with the provided password.
	 *
	 * @since 2.5.0
	 *
	 * @param WP_User|WP_Error $user     WP_User or WP_Error object if a previous
	 *                                   callback failed authentication.
	 * @param string           $password Password to check against the user.
	 */
	return apply_filters( 'wp_authenticate_user', $user, $password );


}