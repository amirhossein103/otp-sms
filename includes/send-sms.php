<?php

add_action( 'wp_ajax_validate_and_action', 'validate_inputs' );
add_action( 'wp_ajax_nopriv_validate_and_action', 'validate_inputs' );
function validate_inputs() {

	check_ajax_referer( 'otp-nonce', 'security', true );

	$errors = '';
	$mobile = kavenegar_arabic_persian_filter($_POST['mobile']);

	try {
		if ( empty( $mobile ) || ! kavenegar_build_same_num( $mobile ) ) {
			throw new Exception("<strong>خطا: </strong>" . __( 'لطفا یک شماره موبایل معتبر وارد کنید.', 'woocommerce' ));
		}

		if ( 'yes' === get_option( 'woocommerce_registration_generate_username', 'yes' ) && empty( $username ) ) {
			$username = kavenegar_build_same_num( $mobile );

		} else {
			$username = $_POST['username'];
			if ( empty( $username ) || ! validate_username( $username ) ) {
				throw new Exception("<strong>خطا: </strong>" . __( 'Please enter a valid account username.', 'woocommerce' ));
			}
		}

		if ( username_exists( $username ) ) {
			throw new Exception("<strong>خطا: </strong>" . __( 'این شماره یا نام کاربری قبلا ثبت شده است.برای وارد شدن از لینک ورود به حساب استفاده کنید.', 'woocommerce' ));
		}

		$mobile= kavenegar_build_standard_num($mobile);

		if ( kavenegar_in_time_limit( $mobile ) ) {
			echo "2";
		} else {

			kavenegar_send_sms( $mobile );

			echo "1";
		}
		die;
	}catch (Exception $e){
		echo '<div class="alert alert-error" role="alert">' . $e->getMessage() . '</div>';
		exit;
	}

}

add_action( 'wp_ajax_nopriv_check_login_info', 'kavenegar_validate_login_data' );
function kavenegar_validate_login_data() {
	check_ajax_referer( 'otp-nonce', 'security', true );

	$errors = '';
	$mobile = kavenegar_arabic_persian_filter($_POST['mobile']);
	try {
		if ( empty( $mobile ) ) {
			throw new Exception('<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'کادر شماره موبایل خالی است.', 'woocommerce' ));
		}

		if ( ! kavenegar_build_same_num( $mobile ) ) {
			throw new Exception("<strong>خطا: </strong>" . __( 'لطفا یک شماره موبایل معتبر وارد کنید.', 'woocommerce' ));
		}
		if ( ! username_exists( kavenegar_build_same_num( $mobile ) ) && ! kavenegar_billing_phone_exist( kavenegar_build_standard_num( $mobile )) ) {

			throw new Exception("<strong>خطا: </strong>" . __( 'شماره موبایل هنوز در وبسایت ثبت نام نکرده است.', 'woocommerce' ));
		}

		$mobile= kavenegar_build_standard_num($mobile);

		if ( kavenegar_in_time_limit( $mobile ) ) {
			echo "2";
		} else {

			kavenegar_send_sms( $mobile );

			echo "1";
		}

		exit;
	} catch ( Exception $exception ) {
		echo '<div class="alert alert-error" role="alert">' .apply_filters( 'kavenegar_login_errors', $exception->getMessage() ) . '</div>';
		exit;
	}

}

add_action( 'wp_ajax_nopriv_resend_otp', 'kavenegar_resend_time_left' );
function kavenegar_resend_time_left() {
	$time_left = '0';

	$mobile = kavenegar_arabic_persian_filter($_POST['mobile']) ?? '';
	$mobile = kavenegar_build_standard_num(kavenegar_sanitize_phone_number( $mobile ));

	if ( ! empty( $mobile ) && kavenegar_in_time_limit( $mobile ) ) {

		$time_field   = kavenegar_in_time_limit( $mobile, true );
		$time_set     = $time_field[1];
		$current_time = strtotime( current_datetime()->format( 'Y-m-d H:i:s' ) );
		$max_time     = $time_set + ( 4 * 60 );

		if ( $max_time - $current_time > 0 ) {
			$time_left = $max_time - $current_time;
		}
	}
	echo $time_left;
	exit;
}

add_action( 'wp_ajax_nopriv_the_resend_link', 'kavenegar_validate_sms_resend_link' );
function kavenegar_validate_sms_resend_link() {

	$mobile = kavenegar_arabic_persian_filter($_POST['mobile']) ?? '';
	$mobile = kavenegar_build_standard_num(kavenegar_sanitize_phone_number( $mobile ));

	$resend_link = "0";

	if ( ! empty( $mobile ) && ! kavenegar_in_time_limit( $mobile ) && kavenegar_build_same_num( $mobile ) ) {
		$resend_link = "1";
	}
	echo $resend_link;
	exit;
}

add_action( 'wp_ajax_nopriv_resend_sms', 'kavenegar_resend_sms' );
function kavenegar_resend_sms() {

	$mobile = kavenegar_arabic_persian_filter($_POST['mobile']) ?? '';
	$mobile = kavenegar_build_standard_num(kavenegar_sanitize_phone_number( $mobile ));

	$resend = "0";

	if ( ! empty( $mobile ) && ! kavenegar_in_time_limit( $mobile ) && kavenegar_build_same_num( $mobile ) ) {
		kavenegar_send_sms( $mobile );
		$resend = "1";
	}
	echo $resend;
	exit;
}