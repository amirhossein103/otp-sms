<?php
defined( 'ABSPATH' ) || exit; //Exit if accessed directly

if ( ! function_exists( 'kavenegar_create_new_customer' ) ) {

	/**
	 * Create a new customer.
	 *
	 * @param  string $mobile    Customer email.
	 * @param  string $username Customer username.
	 * @param  string $password Customer password.
	 * @param  array  $args     List of arguments to pass to `wp_insert_user()`.
	 * @return int|WP_Error Returns WP_Error on failure, Int (user ID) on success.
	 */
	function kavenegar_create_new_customer( $mobile, $username = '', $password = '', $otp_sms, $args = array() ) {
		if ( empty( $mobile ) || ! kavenegar_build_same_num( $mobile ) ) {
			return new WP_Error( 'registration-error-invalid-mobile', __( 'لطفا یک شماره موبایل معتبر وارد کنید.', 'woocommerce' ) );
		}


		if ( 'yes' === get_option( 'woocommerce_registration_generate_username', 'yes' ) && empty( $username ) ) {
			$username = kavenegar_build_same_num($mobile);
		}

		if ( empty( $username ) || ! validate_username( $username ) ) {
			return new WP_Error( 'registration-error-invalid-username', __( 'Please enter a valid account username.', 'woocommerce' ) );
		}

		if ( username_exists( $username ) ) {
			return new WP_Error( 'registration-error-username-exists', __( 'این شماره یا نام کاربری قبلا ثبت شده است.برای وارد شدن از لینک ورود به حساب استفاده کنید.', 'woocommerce' ) );
		}

		// Handle password creation.
		$password_generated = false;
		if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && empty( $password ) ) {
			$password           = wp_generate_password();
			$password_generated = true;
		}

		if ( empty( $password ) ) {
			return new WP_Error( 'registration-error-missing-password', __( 'Please enter an account password.', 'woocommerce' ) );
		}

		$mobile= kavenegar_build_standard_num($mobile);

		if (empty($otp_sms) || !kavenegar_is_valid_otp($otp_sms, $mobile)){
			return new WP_Error( 'registration-error-invalid-code', __( 'کد تایید وارد شده صحیح نمیباشد.', 'woocommerce' ) );
		}

		// Use WP_Error to handle registration errors.
		$errors = new WP_Error();

		do_action( 'woocommerce_register_post', $username, $mobile, $errors );

		$errors = apply_filters( 'woocommerce_registration_errors', $errors, $username, $mobile );

		if ( $errors->get_error_code() ) {
			return $errors;
		}


		$new_customer_data = apply_filters(
			'woocommerce_new_customer_data',
			array_merge(
				$args,
				array(
					'user_mobile' => $mobile,
					'user_login' => $username,
					'user_pass'  => $password,
					'role'       => 'customer',
				)
			)
		);

		$customer_id = wp_insert_user( $new_customer_data );

		if ( is_wp_error( $customer_id ) ) {
			return $customer_id;
		}


		// Update customer object to keep data in sync.
		$customer = new WC_Customer( $customer_id );
		$customer->set_billing_phone( $mobile );
		$customer->save();

		do_action( 'woocommerce_created_customer', $customer_id, $new_customer_data, $password_generated );

		return $customer_id;
	}
}

