<?php
/**
 * Process the registration form.
 */

try {

	class NEW_WC_Meta_Box_Product_Data {

		/**
		 * Process the login form.
		 *
		 * @throws Exception On login error.
		 */
		public static function process_login() {
			$nonce_value = wc_get_var( $_REQUEST['woocommerce-login-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) );

			if ( isset( $_POST['login'], $_POST['mobile'] ) && ( isset( $_POST['otp_sms'] ) || isset( $_POST['password'] ) ) && wp_verify_nonce( $nonce_value, 'woocommerce-login' ) ) {

				$user_login = kavenegar_build_same_num( trim( wp_unslash( $_POST['mobile'] ) ) ) ? kavenegar_build_same_num( trim( wp_unslash( $_POST['mobile'] ) ) ) : trim( wp_unslash( $_POST['mobile'] ) );
				try {
					$pass  = $_POST['password'] ?? $_POST['otp_sms'];
					$creds = array(
						'user_login'    => kavenegar_arabic_persian_filter( $user_login ),
						'user_password' => kavenegar_arabic_persian_filter( $pass ),
						'remember'      => isset( $_POST['rememberme'] ),
					);

					if ( isset( $_POST['otp_sms'] ) ) {

						if ( ! kavenegar_is_valid_otp( kavenegar_arabic_persian_filter( $_POST['otp_sms'] ), kavenegar_build_standard_num( $creds['user_login'] ) ) ) {
							throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'کد تایید وارد شده صحیح نمیباشد.', 'woocommerce' ) );
						}

						remove_filter( 'authenticate', 'wp_authenticate_username_password', 20 );
						remove_filter( 'authenticate', 'wp_authenticate_email_password', 20 );
						remove_filter( 'authenticate', 'wp_authenticate_application_password', 20 );

						add_filter( 'authenticate', 'kavenegar_authenticate_username_otp', 20, 3 );
					}
					$validation_error = new WP_Error();
					$validation_error = apply_filters( 'woocommerce_process_login_errors', $validation_error, $creds['user_login'], $creds['user_password'] );

					if ( $validation_error->get_error_code() ) {
						throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $validation_error->get_error_message() );
					}

					if ( empty( $creds['user_login'] ) ) {
						throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'کادر شماره موبایل خالی است.', 'woocommerce' ) );
					}

					if ( is_email( $creds['user_login'] ) || ! validate_username( $creds['user_login'] ) ) {
						throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'یک شماره موبایل یا نام کاربری معتبر وارد کنید.', 'woocommerce' ) );
					}

					if ( ! username_exists( $creds['user_login'] ) && kavenegar_billing_phone_exist( kavenegar_build_standard_num( $creds['user_login'] ) ) ) {
						$creds['user_login'] = kavenegar_get_user_by_billing_phone( kavenegar_build_standard_num( $creds['user_login'] ) );

						if ( $creds['user_login'] == null ) {

							throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'حساب کاربری برای خریدار قبلی فعال نیست.با مدیریت سایت تماس بگیرید.', 'woocommerce' ) );

						}

					} elseif ( ! username_exists( $creds['user_login'] ) ) {
						throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'این کاربر وجود ندارد.', 'woocommerce' ) );
					}

					// On multisite, ensure user exists on current site, if not add them before allowing login.
					if ( is_multisite() ) {
						$user_data = get_user_by( is_email( $creds['user_login'] ) ? 'email' : 'login', $creds['user_login'] );

						if ( $user_data && ! is_user_member_of_blog( $user_data->ID, get_current_blog_id() ) ) {
							add_user_to_blog( get_current_blog_id(), $user_data->ID, 'customer' );
						}
					}

					// Perform the login.
					$user = wp_signon( apply_filters( 'woocommerce_login_credentials', $creds ), is_ssl() );

					if ( is_wp_error( $user ) ) {
						throw new Exception( $user->get_error_message() );
					} else {

						if ( ! empty( $_POST['redirect'] ) ) {
							$redirect = wp_unslash( $_POST['redirect'] );
						} elseif ( wc_get_raw_referer() ) {
							$redirect = wc_get_raw_referer();
						} else {
							$redirect = wc_get_page_permalink( 'myaccount' );
						}

						wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_login_redirect', remove_query_arg( 'wc_error', $redirect ), $user ), wc_get_page_permalink( 'myaccount' ) ) );
						exit;
					}
				} catch ( Exception $e ) {
					wc_add_notice( apply_filters( 'login_errors', $e->getMessage() ), 'error' );
					do_action( 'woocommerce_login_failed' );
				}
			}
		}

		/**
		 * Process the registration form.
		 *
		 * @throws Exception On registration error.
		 */
		public static function process_registration() {
			$nonce_value = isset( $_POST['_wpnonce'] ) ? wp_unslash( $_POST['_wpnonce'] ) : '';
			$nonce_value = isset( $_POST['woocommerce-register-nonce'] ) ? wp_unslash( $_POST['woocommerce-register-nonce'] ) : $nonce_value;
			if ( isset( $_POST['register'], $_POST['mobile'] ) && wp_verify_nonce( $nonce_value, 'woocommerce-register' ) ) {
				$username = 'no' === get_option( 'woocommerce_registration_generate_username' ) && isset( $_POST['username'] ) ? wp_unslash( $_POST['username'] ) : '';
				$password = 'no' === get_option( 'woocommerce_registration_generate_password' ) && isset( $_POST['password'] ) ? $_POST['password'] : '';
				$mobile   = kavenegar_sanitize_phone_number( $_POST['mobile'] );
				$mobile   = kavenegar_arabic_persian_filter( $mobile );
				$otp_sms  = kavenegar_arabic_persian_filter( $_POST['otp_sms'] );

				try {
					$validation_error  = new WP_Error();
					$validation_error  = apply_filters( 'woocommerce_process_registration_errors', $validation_error, $username, $password, $mobile, $otp_sms );
					$validation_errors = $validation_error->get_error_messages();

					if ( 1 === count( $validation_errors ) ) {
						throw new Exception( $validation_error->get_error_message() );
					} elseif ( $validation_errors ) {
						foreach ( $validation_errors as $message ) {
							wc_add_notice( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $message, 'error' );
						}
						throw new Exception();
					}

					$new_customer = kavenegar_create_new_customer( $mobile, wc_clean( $username ), $password, $otp_sms );


					if ( is_wp_error( $new_customer ) ) {
						throw new Exception( $new_customer->get_error_message() );
					}

					if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) ) {
						wc_add_notice( __( 'حساب شما با موفقیت ایجاد شد و رمز عبور به شماره موبایل شما ارسال گردید.', 'woocommerce' ) );
					} else {
						wc_add_notice( __( 'حساب شما با موفقیت ایجاد شد و اطلاعات آن به شماره موبایل شما اراسال گردید.', 'woocommerce' ) );
					}

					// Only redirect after a forced login - otherwise output a success notice.
					if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
						wc_set_customer_auth_cookie( $new_customer );

						if ( ! empty( $_POST['redirect'] ) ) {
							$redirect = wp_sanitize_redirect( wp_unslash( $_POST['redirect'] ) );
						} elseif ( wc_get_raw_referer() ) {
							$redirect = wc_get_raw_referer();
						} else {
							$redirect = wc_get_page_permalink( 'myaccount' );
						}

						wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_registration_redirect', $redirect ), wc_get_page_permalink( 'myaccount' ) ) ); //phpcs:ignore WordPress.Security.SafeRedirect.wp_redirect_wp_redirect
						exit;
					}
				} catch ( Exception $e ) {
					if ( $e->getMessage() ) {
						wc_add_notice( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $e->getMessage(), 'error' );
					}
				}
			}
		}
	}

	if ( class_exists( "WooCommerce" ) ) {
		include_once( kavenegar_plugin_path() . "woocommerce/includes/class-wc-form-handler.php" );
		remove_action( 'wp_loaded', 'WC_Form_Handler::process_login', 20 );
		remove_action( 'wp_loaded', 'WC_Form_Handler::process_registration', 20 );
	}
	add_action( 'wp_loaded', 'NEW_WC_Meta_Box_Product_Data::process_registration', 10 );

	add_action( 'wp_loaded', 'NEW_WC_Meta_Box_Product_Data::process_login', 15 );

} catch ( Exception $e ) {
}