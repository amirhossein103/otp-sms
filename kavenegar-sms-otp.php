<?php
/**
 * Plugin Name: Kavenegar sms otp
 * Plugin URI: http://pyramidwordpress.com
 * Description: A really simple way to add otp authorization to woocommerce default login and registration form.
 * Version: 1.0.1
 * Author: Amirhossein Lalehei
 * Author URI: http://pyramidwordpress.com
 * License: GPL2
 */

defined( 'ABSPATH' ) || exit; //Exit if accessed directly



if (!class_exists('KaveNegar_Handler')){
	class KaveNegar_Handler{
		/**
		 * A reference to an instance of this class.
		 *
		 * @since  1.0.0
		 * @access private
		 * @var    object
		 */
		private static $instance = null;

		/**
		 * Plugin version
		 *
		 * @var string
		 */
		private $version = '1.0.1';

		/**
		 * Holder for base plugin URL
		 *
		 * @since  1.0.0
		 * @access private
		 * @var    string
		 */
		private $plugin_url = null;

		/**
		 * Holder for base plugin path
		 *
		 * @since  1.0.0
		 * @access private
		 * @var    string
		 */
		private $plugin_path = null;

		/**
		 * Sets up needed actions/filters for the plugin to initialize.
		 *
		 * @return void
		 * @since  1.0.0
		 * @access public
		 */
		public function __construct() {
			// Load files.
			add_action( 'init', array( $this, 'init' ), 99 );

			// Enqueue Scripts and styles
			add_action( 'wp_enqueue_scripts', array($this,'enqueue_scripts_styles') );

			// Register activation and deactivation hook.
			register_activation_hook( __FILE__, array( $this, 'activation' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivation' ) );
		}

		/**
		 * Returns plugin version
		 *
		 * @return string
		 */
		public function get_version() {
			return $this->version;
		}

		/**
		 * Manually init required modules
		 */
		public function init() {
			if ( class_exists( 'WooCommerce' ) ) {
				$this->load_files();
			}
			add_theme_support( 'custom-logo', array(
				'height' => 52,
				'width'  => 180,
			) );
		}

		/**
		 * Load required files.
		 */
		public function load_files() {

			require $this->plugin_path( 'gateways/kavenegar/autoload.php' );
			require $this->plugin_path( 'includes/functions.php' );
			require $this->plugin_path( 'includes/register.php' );
			require $this->plugin_path( 'includes/woocommerce-registration.php' );
			require $this->plugin_path( 'includes/send-sms.php' );

		}

		function enqueue_scripts_styles() {
			$ver = $this->get_version();

			wp_register_script( 'kavenegar-js', $this->plugin_url( 'assets/js/kavenegar-scripts.js' ), array( 'jquery' ), $ver, true );
			wp_register_style( 'kavenegar-css', $this->plugin_url( 'assets/css/kavenegar-styles.css' ), array(), $ver );

			wp_localize_script( 'kavenegar-js', 'kavenegar', array(
				'ajax_url'  => esc_url( admin_url( 'admin-ajax.php' ) ),
				'otp_nonce' => wp_create_nonce( 'otp-nonce' ),
				'flag' => false,
			) );

			if ( is_account_page() && ! is_user_logged_in() ) {
				wp_enqueue_style( 'kavenegar-css' );
				wp_enqueue_script( 'kavenegar-js' );
			}
		}
		/**
		 * Returns path to file or dir inside plugin folder
		 *
		 * @param string $path Path inside plugin dir.
		 *
		 * @return string
		 */
		public function plugin_path( $path = null ) {

			if ( ! $this->plugin_path ) {
				$this->plugin_path = trailingslashit( plugin_dir_path( __FILE__ ) );
			}

			return $this->plugin_path . $path;

		}

		/**
		 * Returns url to file or dir inside plugin folder
		 *
		 * @param string $path Path inside plugin dir.
		 *
		 * @return string
		 */
		public function plugin_url( $path = null ) {

			if ( ! $this->plugin_url ) {
				$this->plugin_url = trailingslashit( plugin_dir_url( __FILE__ ) );
			}

			return $this->plugin_url . $path;

		}
		/**
		 * Do some stuff on plugin activation
		 *
		 * @return void
		 * @since  1.0.0
		 */
		public function activation() {
			global $wpdb;
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

			$charset_collate = $wpdb->get_charset_collate();

			$table_name = $wpdb->prefix . 'otp_sms';
			if ($wpdb->get_var("show tables like '{$table_name}'") != $table_name) {
				$create_sms_send = ("CREATE TABLE IF NOT EXISTS {$table_name}(
            ID int(10) NOT NULL auto_increment,
            date DATETIME,
            recipient TEXT NOT NULL,
  			token TEXT NOT NULL,
            PRIMARY KEY(ID)) $charset_collate");

				dbDelta($create_sms_send);
			}
		}

		/**
		 * Do some stuff on plugin activation
		 *
		 * @return void
		 * @since  1.0.0
		 */
		public function deactivation() {
		}

		/**
		 * Returns the instance.
		 *
		 * @return object
		 * @since  1.0.0
		 * @access public
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;

		}
	}
}
if ( ! function_exists( 'kavenegar_sms' ) ) {
	function kavenegar_sms() {
		return KaveNegar_Handler::get_instance();
	}
}
kavenegar_sms();