<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="mrgn-form-login">
    <div class="d-flex ai-center jc-between">
        <div class="backward-link">
            <a class="d-flex ai-center color-primary" href="<?php echo home_url( '/' ) ?>" title="<?php echo get_bloginfo( 'name' ); ?>" rel="home">
                <span class="fa fa-arrow-right" style="margin-left: 3px;"></span>
                <span>بازگشت به فروشگاه</span>
            </a>
        </div>
        <div class="logo-site-wrap">
            <?php $custom_logo = get_theme_mod('custom_logo');
            $logo_src = wp_get_attachment_image_src($custom_logo,'full');
            ?>
            <img src="<?php echo esc_url($logo_src[0]);?>" alt="<?php echo get_bloginfo( 'name' ); ?>" height="52" width="150" />
        </div>
    </div>
<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

    <?php
    $is_registration_form = isset( $_GET['action'] );
    $is_otpLogin          = ! isset( $_GET['login'] );

    $redirect_param = $_GET['redirect_to'] ?? '';

    $link = wc_get_page_permalink('myaccount');

    if ( $redirect_param ) {
	    $link = add_query_arg( array( 'redirect_to' => $redirect_param ), $link );
    }
    $display = $is_otpLogin?"none":"block";

    if (!$is_registration_form): ?>
        <h2><?php esc_html_e( 'Login', 'woocommerce' ); ?></h2>
        <form class="woocommerce-form woocommerce-form-login clearfix login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="mobile"><?php esc_html_e( 'شماره موبایل', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="mobile" id="mobile" autocomplete="mobile" value="<?php echo ( ! empty( $_POST['mobile'] ) ) ? esc_attr( wp_unslash( $_POST['mobile'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
			</p>
	        <?php if (!$is_otpLogin):?>
			<p  class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-mrgn-password">
				<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
			</p>
	        <?php endif;?>
	        <?php if ($is_otpLogin):?>
                <p class="woocommerce-form-row form-row">
                    <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-login__submit" name="login_otp" value="<?php esc_attr_e( 'ورود با رمز یکبار مصرف', 'woocommerce' ); ?>"><?php esc_html_e( 'ورود با رمز یکبار مصرف', 'woocommerce' ); ?></button>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-code-otp" style="display: none;">
                    <label for="otp_sms"><?php esc_html_e( 'کد تایید', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="otp_sms" id="otp_sms" autocomplete="off" />
                </p>
                <div class="resend-otp" style="display: none;">
                    <div class="countdown-warp">
                        <span>ارسال مجدد کد تایید:</span>
                        (
                        <span id="count__down">0</span>
                        )
                    </div>
                    <div  data-action="0" class="disabled-resend" id="resend-sms">
                        ارسال دوباره!
                        <span class="loading-icon"></span>
                    </div>
                </div>
                <a href="<?php echo add_query_arg(array('login'=>'pass'),$link);?>" id="login-by-password"><span>ورود با گذرواژه</span></a>
	        <?php endif;?>
			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-row">
				<?php if (!$is_otpLogin):?>
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
				    </label>
				<?php endif;?>

                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<button type="submit" style="display: <?php esc_attr_e($display);?>" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
			</p>
            <?php if (!$is_otpLogin):?>
            <div class="d-flex ai-center jc-between">
                <p class="woocommerce-LostPassword lost_password">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">بازیابی گذرواژه</a>
                </p>
                <a href="<?php echo remove_query_arg('login',$link);?>" id="login-by-otp" class="mr-small"><span>ورود با رمز یکبار مصرف</span></a>
            </div>
            <?php endif;?>
			<?php do_action( 'woocommerce_login_form_end' ); ?>
		</form>
        <div id="show-registration-form" class="registration-form-action">
            <div class="d-flex ai-center jc-between">
                <p class="mb-0">حساب کاربری ندارید؟</p>
                <a href="<?php echo add_query_arg(array('action'=>'register'),$link);?>" class="d-flex ai-center"><span>ایجاد حساب</span><span class="fa fa-arrow-left mr-small"></span></a>
            </div>
        </div>

    <?php else:?>
        <h2><?php esc_html_e( 'Register', 'woocommerce' ); ?></h2>
        <form method="post" class="woocommerce-form woocommerce-form-register clearfix register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

	        <?php do_action('Otamis/before_password/registration');?>
		    <?php do_action( 'woocommerce_register_form_start' ); ?>

		    <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                </p>

		    <?php endif; ?>

            <p class="form-row form-row-wide">
                <label for="reg_mobile"><?php _e( 'شماره موبایل', 'woocommerce' ); ?><span class="required">*</span></label>
                <input type="text" class="input-text" name="mobile" id="reg_mobile" value="<?php echo ( ! empty( $_POST['mobile'] ) ) ? esc_attr( wp_unslash( $_POST['mobile'] ) ) : ''; ?>" />
            </p>

		    <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
                </p>


		    <?php else : ?>

                <p><?php esc_html_e( 'گذرواژه به شماره موبایل شما ارسال خواهد شد.', 'woocommerce' ); ?></p>

		    <?php endif; ?>


            <?php do_action( 'woocommerce_register_form' ); ?>

            <p class="woocommerce-form-row form-row">
                <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="send_otp" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
            </p>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" style="display: none;">
                <label for="otp_sms"><?php esc_html_e( 'کد تایید', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="otp_sms" id="otp_sms" autocomplete="off" />
            </p>

            <div class="resend-otp" style="display: none;">
                <div class="countdown-warp">
                    <span>ارسال مجدد کد تایید:</span>
                    (
                    <span id="count__down">0</span>
                    )
                </div>
                <div  data-action="0" class="disabled-resend" id="resend-sms">
                    ارسال دوباره!
                    <span class="loading-icon"></span>
                </div>
            </div>
            <p class="woocommerce-form-row form-row" style="display: none;">
			    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                <button type="submit" class="let-full woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'ادامه', 'woocommerce' ); ?></button>
            </p>

		    <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>
        <div id="show-login-form" class="d-flex ai-center jc-between">
            <p class="mb-0">حساب کاربری دارید؟</p>
            <a href="<?php echo remove_query_arg('action',$link);?>" class="d-flex ai-center"><span>ورود به حساب</span><span class="fa fa-arrow-left mr-small"></span></a>
        </div>
    <?php endif; ?>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
</div>